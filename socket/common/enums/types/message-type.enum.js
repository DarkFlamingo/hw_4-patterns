const MessageType = {
  START: 'start',
  START_RACE: 'startRace',
  PROGRESS: 'progress',
  APPROACHING_FINISH_LINE: 'approachingFinishLine',
  FINISH_LINE: 'finishLine',
  FINISH_GAME: 'finishGame',
};

export { MessageType };
