import ResultRepository from '../repositories/result.repository';
import * as config from '../config';

class ResultService {
  constructor(ResultRepository) {
    this._ResultRepository = ResultRepository;
  }

  getAllResults() {
    return this._ResultRepository.getAll();
  }

  getResultById(resultId) {
    return this._ResultRepository.getResultById(resultId);
  }

  addResult(result) {
    return this._ResultRepository.add(result);
  }

  deleteResult(resultId) {
    return this._ResultRepository.delete(resultId);
  }

  updateResult(resultId, newData) {
    return this._ResultRepository.update(resultId, newData);
  }

  isCharactersToTheFinish(resultId) {
    const result = this.getResultById(resultId);
    return (
      result.keyAllCount - result.keyCount ===
      config.NUMBER_OF_LETTERS_TO_THE_END
    );
  }
}

export default new ResultService(ResultRepository);
