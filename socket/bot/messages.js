import { transports } from '../../data';
import { MessageType } from '../common/enums/enums';

function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min)) + min;
}

const getRandomTransport = () => {
  if (transports.length) {
    return transports[getRandomInt(0, transports.length)];
  } else {
    return null;
  }
};

const messages = {
  start: () => 'Welcome to the race!',

  startRace: (users) => {
    let resultStr = 'So today the following players take part in this race: ';
    users.forEach(
      (user, index) =>
        (resultStr += `\n ${index + 1}) ${
          user.id
        }. This user is using a ${getRandomTransport()}. He has ${getRandomInt(
          0,
          100
        )} victories`)
    );
    return resultStr;
  },

  progress: (results) => {
    let resultStr = 'At the moment the situation is as follows: ';
    results.sort((a, b) => {
      return -(a.keyCount - b.keyCount);
    });
    results.forEach(
      (result, index) =>
        (resultStr += `\n ${index + 1}) ${result.id}. Progress ${
          result.keyCount
        }`)
    );
    return resultStr;
  },

  approachingFinishLine: (username) => {
    let resultStr = `The invincible ${username} is already approaching the finish line`;
    return resultStr;
  },

  finishLine: (username) => {
    let resultStr = `${username} crosses the finish line`;
    return resultStr;
  },

  finishGame: (results) => {
    let resultStr = `This race ended with the following results: `;
    results.sort((a, b) => {
      return -(a.keyCount - b.keyCount);
    });
    results.forEach((result, index) => {
      resultStr += `\n ${index + 1}) ${result.id}. Time: ${result.time}`;
    });
    return resultStr;
  },
};

const MessageFactory = () => {
  return {
    createMessage: (type, arg = null) => {
      switch (type) {
        case MessageType.START:
          return messages.start();
        case MessageType.START_RACE:
          return messages.startRace(arg);
        case MessageType.PROGRESS:
          return messages.progress(arg);
        case MessageType.APPROACHING_FINISH_LINE:
          return messages.approachingFinishLine(arg);
        case MessageType.FINISH_LINE:
          return messages.finishLine(arg);
        case MessageType.FINISH_GAME:
          return messages.finishGame(arg);
      }
    },
  };
};

const MessageProxy = () => {
  const Message = MessageFactory();

  return {
    createMessageProxy: (type, arg = null) => {
      if (type in messages) {
        return Message.createMessage(type, arg);
      } else {
        throw new Error('There is no such type in messages-type.enum');
      }
    },
  };
};

export { MessageProxy };
