export const getTextForRacing = async (id) => {
  return await fetch(`/game/texts/${id}`).then((res) => res.text());
};
