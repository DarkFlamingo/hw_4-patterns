class RoomRepository {
  constructor() {
    this._ROOMS = [];
  }

  getAll() {
    return this._ROOMS;
  }

  getRoomById(id) {
    const index = this._ROOMS.findIndex((room) => room.id === id);
    if (index !== -1) {
      return this._ROOMS[index];
    }
    return null;
  }

  delete(id) {
    this._ROOMS = this._ROOMS.filter((room) => room.id !== id);
    return this._ROOMS;
  }

  add(id) {
    this._ROOMS.push({
      id,
      users: [],
      isActive: true,
      inGame: false,
      bot: null,
    });
    return this.getRoomById(id);
  }

  update(id, newData) {
    this._ROOMS = this._ROOMS.map((room) =>
      room.id !== id ? room : { ...newData }
    );
    return this.getRoomById(id);
  }
}

export default new RoomRepository();
