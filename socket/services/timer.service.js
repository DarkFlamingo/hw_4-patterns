class Timer {
  constructor() {
    this._TIMER = null;
    this.start = null;
  }

  startTimer(timeSeconds, cb, cb_2) {
    this._TIMER = setTimeout(() => {
      if (timeSeconds <= 0) {
        cb_2();
      } else {
        --timeSeconds;
        this.startTimer(timeSeconds, cb, cb_2);
        cb(timeSeconds);
      }
    }, 1000);
  }

  stopTimer() {
    clearTimeout(this._TIMER);
    this._TIMER = null;
    this.start = null;
  }

  setStart() {
    this.start = Date.now();
  }

  getTime() {
    return Math.floor((Date.now() - this.start) / 1000);
  }
}

export default new Timer();
