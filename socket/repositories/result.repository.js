class ResultRepository {
  constructor() {
    this._RESULTS = [];
  }

  getAll() {
    return this._RESULTS;
  }

  getResultById(id) {
    const index = this._RESULTS.findIndex((result) => result.id === id);
    if (index !== -1) {
      return this._RESULTS[index];
    }
    return null;
  }

  delete(id) {
    this._RESULTS = this._RESULTS.filter((result) => result.id !== id);
    return this._RESULTS;
  }

  add({ id, keyCount, keyAllCount, time }) {
    this._RESULTS.push({ id, keyCount, keyAllCount, time });
    return this.getResultById(id);
  }

  update(id, newData) {
    this._RESULTS = this._RESULTS.map((result) =>
      result.id !== id ? result : { ...newData }
    );
    return this.getResultById(id);
  }
}

export default new ResultRepository();
