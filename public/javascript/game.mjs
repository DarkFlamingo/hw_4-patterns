import { createElement, addClass, removeClass } from './helpers/dom.helper.mjs';
import { getTextForRacing } from './helpers/api.helper.mjs';
import {
  createWinnerDiv,
  createLetterSpan,
  createUserDiv,
  createQuitButton,
  createRoomItem,
} from './helpers/domElement.helper.mjs';

const username = sessionStorage.getItem('username');
let text = null;
let textArray = null;
let currentLetter = 0;
let activeRoom = null;

if (!username) {
  window.location.replace('/login');
}

const socket = io('ws://localhost:3002', { query: { username } });

const roomsPage = document.getElementById('rooms-page');
const gamePage = document.getElementById('game-page');
const roomsList = document.getElementById('rooms-list');
const addRoomBtn = document.getElementById('add-room-btn');
const infoBlock = document.getElementById('info-block');
const readyButton = document.getElementById('ready-btn');
const timer = document.getElementById('timer');
const textContainer = document.getElementById('text-container');
const gameTimer = document.getElementById('game-timer');
const quitGameBtn = document.getElementById('quit-results-btn');
const winners = document.getElementById('winners');
const winnersContainer = document.getElementById('winners-container');
const botText = document.getElementById('bot-text');

const setActiveRoomId = (room, users) => {
  activeRoom = room;
  if (activeRoom) {
    addClass(roomsPage, 'display-none');
    removeClass(gamePage, 'display-none');
    renderGamePage(room, users);
  } else {
    removeClass(roomsPage, 'display-none');
    addClass(gamePage, 'display-none');
  }
};

const renderGamePage = (room, users) => {
  if (!room.inGame) {
    const roomName = document.createElement('h1', '', { id: 'room-name' });
    roomName.innerText = room.id;
    const quitButton = createQuitButton(room.id, () => {
      socket.emit('QUIT', room.id);
    });
    const userContainer = createElement('div', '', { id: 'users-container' });
    const userItems = users.map((el) => createUserDiv(el, username));
    userItems.forEach((item) => userContainer.appendChild(item));
    infoBlock.innerHTML = '';
    infoBlock.appendChild(roomName);
    infoBlock.appendChild(quitButton);
    infoBlock.appendChild(userContainer);
  } else {
    const deletedUsers = Array.prototype.slice
      .call(document.getElementById('users-container').children)
      .filter((item) =>
        hasNotUser(activeRoom.users, item.getAttribute('id').substr(10))
      )
      .map((el) => el.getAttribute('id').substr(10));
    deletedUsers.forEach((el) => {
      document
        .getElementById('users-container')
        .removeChild(document.getElementById(`users-div-${el}`));
    });
  }
};

const hasNotUser = (users, username) => {
  return users.every((item) => item !== username);
};

const updateRooms = (rooms) => {
  roomsList.innerHTML = '';
  const roomItems = rooms.map((el) =>
    createRoomItem(el.id, el.users.length, () => {
      socket.emit('JOIN_ROOM', el.id);
    })
  );
  roomItems.forEach((item) => roomsList.appendChild(item));
};

const roomExist = () => {
  alert('Room already exists');
};

const userExist = () => {
  alert(
    'User with such username already exists. Please enter another username'
  );
  sessionStorage.removeItem('username');
  window.location.replace('/login');
};

const loadCurrentRoom = (data) => {
  if (data) {
    const { room, users } = data;
    setActiveRoomId(room, users);
  } else {
    setActiveRoomId(null, null);
  }
};

const prepareTimer = (id) => {
  const quitButton = document.getElementById('quit-room-btn');
  addClass(quitButton, 'display-none');

  getTextForRacing(id).then((data) => (text = data));

  showTimer();
  socket.emit('START_TIMER', activeRoom.id);
};

const startGame = () => {
  showGameWindow();
  document.addEventListener('keydown', onKeyPress);
  textArray = transformTextForGame(text);
  currentLetter = 0;
  if (textArray[currentLetter]) addClass(textArray[currentLetter], 'next');
  updateTextInGame(textArray);
  socket.emit('START_GAME_TIMER', activeRoom.id);
};

const updateTextInGame = (array) => {
  textContainer.innerText = '';
  array.forEach((item) => textContainer.appendChild(item));
};

const workTimer = (seconds) => {
  timer.innerText = seconds;
};

const workGameTimer = (seconds) => {
  gameTimer.innerText = seconds;
};

const showReadyBtn = () => {
  removeClass(readyButton, 'display-none');
  addClass(timer, 'display-none');
  addClass(textContainer, 'display-none');
  addClass(gameTimer, 'display-none');
  addClass(winnersContainer, 'display-none');
  addClass(quitGameBtn, 'display-none');
};

const showGameWindow = () => {
  addClass(readyButton, 'display-none');
  addClass(timer, 'display-none');
  removeClass(textContainer, 'display-none');
  removeClass(gameTimer, 'display-none');
  addClass(winnersContainer, 'display-none');
  addClass(quitGameBtn, 'display-none');
};

const showTimer = () => {
  addClass(readyButton, 'display-none');
  removeClass(timer, 'display-none');
  addClass(textContainer, 'display-none');
  addClass(gameTimer, 'display-none');
  addClass(winnersContainer, 'display-none');
  addClass(quitGameBtn, 'display-none');
};

const showModalWindow = () => {
  addClass(readyButton, 'display-none');
  addClass(timer, 'display-none');
  addClass(textContainer, 'display-none');
  addClass(gameTimer, 'display-none');
  removeClass(winnersContainer, 'display-none');
  removeClass(quitGameBtn, 'display-none');
};

const updateProgressBar = ({ username, percent }) => {
  const progressBar = document.getElementsByClassName(
    `user-progress ${username}`
  )[0];
  if (percent === 100) {
    addClass(progressBar, 'finished');
  }
  progressBar.style.width = `${percent}%`;
  isEveryOneIsDone();
};

const transformTextForGame = (text) => {
  const lettersArray = text.split('');
  const transformedArray = lettersArray.map((el) => createLetterSpan(el));
  return transformedArray;
};

const isEveryOneIsDone = () => {
  const progressBarArray = Array.prototype.slice.call(
    document.getElementsByClassName('user-progress')
  );
  if (progressBarArray.every((item) => item.classList.contains('finished')))
    socket.emit('EVERYONE_DONE', activeRoom.id);
};

const onClickAddBtn = () => {
  const roomName = window.prompt("Enter room's name", 'Name');
  if (roomName) {
    socket.emit('CREATE_ROOM', roomName);
  }
};

const onClickReadyBtn = () => {
  socket.emit('USER_READY', activeRoom.id);
  removeClass(readyButton, 'btn-success');
  addClass(readyButton, 'btn-danger');
  readyButton.removeEventListener('click', onClickReadyBtn);
  readyButton.addEventListener('click', onClickNotReadyBtn);
  readyButton.innerText = 'Not Ready';
};

const onClickNotReadyBtn = () => {
  socket.emit('USER_NOT_READY', activeRoom.id);
  removeClass(readyButton, 'btn-danger');
  addClass(readyButton, 'btn-success');
  readyButton.removeEventListener('click', onClickNotReadyBtn);
  readyButton.addEventListener('click', onClickReadyBtn);
  readyButton.innerText = 'Ready';
};

const onKeyPress = (event) => {
  const letter = event.key;
  if (textArray[currentLetter]) {
    if (textArray[currentLetter].textContent === letter) {
      addClass(textArray[currentLetter], 'done');
      removeClass(textArray[currentLetter], 'next');
      if (textArray[currentLetter + 1])
        addClass(textArray[currentLetter + 1], 'next');
      let progress;
      if (textArray && textArray.length !== 0) {
        progress = (currentLetter / (textArray.length - 1)) * 100;
      }
      socket.emit('PROGRESS_BAR_CHANGED', {
        room: activeRoom,
        username,
        keyCount: currentLetter,
        keyAllCount: textArray.length - 1,
      });
      currentLetter++;
    }
  }
};

const finishGame = (winnersArray) => {
  stopTimer();
  showModalWindow();
  quitGameBtn.addEventListener('click', restartGame);
  document.removeEventListener('keydown', onKeyPress);
  if (winnersArray) {
    const winnerItemsArray = winnersArray.map((item, index) =>
      createWinnerDiv(item, index + 1)
    );
    winners.innerHTML = '';
    winnerItemsArray.forEach((item) => winners.appendChild(item));
  }
};

const restartGame = () => {
  quitGameBtn.removeEventListener('click', restartGame);
  onClickNotReadyBtn();
  showReadyBtn();
  text = null;
  textArray = null;
  currentLetter = 0;
  socket.emit('RESET_GAME', activeRoom.id);
};

const stopTimer = () => {
  socket.emit('STOP_TIMER');
};

const setComment = (msg) => {
  botText.innerText = msg;
};

socket.on('USER_EXIST', userExist);
socket.on('ROOM_EXIST', roomExist);
socket.on('UPDATE_ROOMS', updateRooms);
socket.on('JOIN_ROOM_DONE', loadCurrentRoom);
socket.on('QUIT_DONE', loadCurrentRoom);
socket.on('EVERY_USER_READY', prepareTimer);
socket.on('START_GAME', startGame);
socket.on('TIMER_BEFORE', workTimer);
socket.on('GAME_TIMER', workGameTimer);
socket.on('UPDATE_PROGRESS_BAR_FRONT', updateProgressBar);
socket.on('FINISH_GAME', finishGame);
socket.on('COMMENT', setComment);

addRoomBtn.addEventListener('click', onClickAddBtn);
readyButton.addEventListener('click', onClickReadyBtn);
