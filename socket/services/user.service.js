import UserRepository from '../repositories/user.repository';

class UserService {
  constructor(UserRepository) {
    this._UserRepository = UserRepository;
  }

  getAllUsers() {
    return this._UserRepository.getAll();
  }

  getUserById(userId) {
    return this._UserRepository.getUserById(userId);
  }

  addUser(id) {
    return this._UserRepository.add(id);
  }

  deleteUser(id) {
    return this._UserRepository.delete(id);
  }

  updateUser(id, newData) {
    return this._UserRepository.update(id, newData);
  }

  isUserExist(id) {
    const user = this._UserRepository.getUserById(id);
    if (user) {
      return true;
    } else {
      return false;
    }
  }

  toggleUserIsReady(id, ready) {
    const user = this._UserRepository.getUserById(id);
    this._UserRepository.update(id, { ...user, isReady: ready });
  }
}

export default new UserService(UserRepository);
