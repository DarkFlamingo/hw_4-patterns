export const texts = [
  "Queen Elizabeth has marked life and times in Britain for the past seven decades, and when she dies, Britain will change. It will be like losing the nation's  grandmother, since the Queen is part of national life, part of the national family, and a very popular figure. Opinion polls in 2020 showed that over 80% of people in Britain appreciate the Queen.",
  'Prince Charles is the heir to the throne, so he will become King unless he decides not to. While Charles is not unpopular, he is less popular than his mother the Queen, and less popular than his son Prince William. He will probably choose to become King, but he may decide that he is too old to start a new job. Charles was born in 1948, so by 2026, when the Queen reaches 100, he will be 78. He may think that at 78 he should be stopping work, not taking on a big new career as Head of State. Yet he may look round and see that Joe Biden became president of the USA at the age of 78, and decide that 78 is a good age to take on the job. After all, being President of the USA is a harder job than being King of England; the President of the USA must take political decisions, the King of England cannot take part in politics.',
  "While nobody knows for certain who Britain's next king will be, it is fairly certain that Britain will remain a monarchy after the Queen dies. Both Charles and William carry out plenty of royal duties, so they know the ropes, and both are prepared to be king. Besides, few people  want Britain to become a republic. A poll in 2012 showed that 80% of people in Britain wanted the monarchy to continue, with only 13% wanting a republic. Perhaps this is understandable; in Britain, as in many other countries, politicians are not particularly popular these days! God save the King !",
];

export const transports = [
  'Ferrari',
  'BMW',
  'Mercedes',
  'Tesla',
  'Toyota',
  'Maserati',
  'Dodge',
  'Mazda',
  'Ford Mustang',
  'McLaren',
  'Volkswagen',
  'Lexus',
];
export default { texts };
