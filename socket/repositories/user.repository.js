class UserRepository {
  constructor() {
    this._USER = [];
  }

  getAll() {
    return this._USER;
  }

  getUserById(id) {
    const index = this._USER.findIndex((user) => user.id === id);
    if (index !== -1) {
      return this._USER[index];
    }
    return null;
  }

  delete(id) {
    this._USER = this._USER.filter((user) => user.id !== id);
    return this._USER;
  }

  add(id) {
    this._USER.push({ id, isReady: false });
    return this.getUserById(id);
  }

  update(id, newData) {
    this._USER = this._USER.map((user) =>
      user.id !== id ? user : { ...newData }
    );
  }
}

export default new UserRepository();
