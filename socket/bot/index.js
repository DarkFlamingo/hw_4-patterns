import RoomService from '../services/room.service';
import ResultService from '../services/result.services';
import * as config from '../config';
import { MessageProxy } from './messages';
import { MessageType } from '../common/enums/enums';

const Message = MessageProxy();

const botComentator = (io, roomId) => {
  let timer;

  return {
    start: () => {
      if (RoomService.getRoomById(roomId)) {
        io.to(roomId).emit('COMMENT', Message.createMessageProxy(MessageType.START));
      }
    },

    sendStartMessage: () => {
      io.to(roomId).emit('COMMENT', Message.createMessageProxy(MessageType.START));
    },

    startRace: (users) => {
      io.to(roomId).emit(
        'COMMENT',
        Message.createMessageProxy(MessageType.START_RACE, users)
      );
    },

    notificationOfProgressOn: () => {
      timer = setInterval(() => {
        io.to(roomId).emit(
          'COMMENT',
          Message.createMessageProxy(
            MessageType.PROGRESS,
            RoomService.getRoomById(roomId).users.map((user) =>
              ResultService.getResultById(user)
            )
          )
        );
      }, config.MESSAGES_DELAY);
    },

    notificationOfProgressOff: () => {
      clearInterval(timer);
    },

    notifyAboutApproachingFinishLine: (username) => {
      io.to(roomId).emit(
        'COMMENT',
        Message.createMessageProxy(MessageType.APPROACHING_FINISH_LINE, username)
      );
    },

    notifyFinish: (username) => {
      io.to(roomId).emit(
        'COMMENT',
        Message.createMessageProxy(MessageType.FINISH_LINE, username)
      );
    },

    notifyFinishGame: () => {
      io.to(roomId).emit(
        'COMMENT',
        Message.createMessageProxy(
          MessageType.FINISH_GAME,
          RoomService.getRoomById(roomId).users.map((user) =>
            ResultService.getResultById(user)
          )
        )
      );
    },
  };
};

export { botComentator };
