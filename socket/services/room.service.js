import RoomRepository from '../repositories/room.repository';
import UserRepository from '../repositories/user.repository';
import * as config from '../config';
/*
{
  id: string,
  users: string[], (Array of id)
  isActive: boolean,
  inGame: boolean
  winners: []
}
*/
class RoomService {
  constructor(RoomRepository) {
    this._RoomRepository = RoomRepository;
    this.winners = new Map();
  }

  getAllRooms() {
    return this._RoomRepository.getAll();
  }

  getRoomById(roomId) {
    return this._RoomRepository.getRoomById(roomId);
  }

  addRoom(roomId) {
    return this._RoomRepository.add(roomId);
  }

  deleteRoom(roomId) {
    return this._RoomRepository.delete(roomId);
  }

  updateRoom(roomId, newData) {
    return this._RoomRepository.update(roomId, newData);
  }

  getAllActiveRooms() {
    return this._RoomRepository
      .getAll()
      .filter((item) => item.isActive === true);
  }

  getAllUsersInRoom(roomId) {
    return this.getRoomById(roomId).users.map((item) =>
      UserRepository.getUserById(item)
    );
  }

  isEmptyRoom(roomId) {
    return this._RoomRepository.getRoomById(roomId).users.length === 0;
  }

  isEveryUserReady(roomId) {
    const room = this._RoomRepository.getRoomById(roomId);
    return room.users.every(
      (el) => UserRepository.getUserById(el).isReady === true
    );
  }

  toggleRoomActive(roomId, active) {
    const room = this._RoomRepository.getRoomById(roomId);
    this._RoomRepository.update(roomId, { ...room, isActive: active });
  }

  toggleRoomInGame(roomId, game) {
    const room = this._RoomRepository.getRoomById(roomId);
    this._RoomRepository.update(roomId, { ...room, inGame: game });
  }

  getRoomByUserId(userId) {
    const rooms = this._RoomRepository.getAll();
    for (let i = 0; i < rooms.length; i++) {
      for (let j = 0; j < rooms[i].users.length; j++) {
        if (rooms[i].users[j] === userId) {
          return rooms[i];
        }
      }
    }
    return null;
  }

  resetIsReady(roomId) {
    const room = this._RoomRepository.getRoomById(roomId);
    room.users.forEach((user) =>
      UserRepository.update(user.id, { ...user, isReady: false })
    );
  }

  isRoomInGame(roomId) {
    const room = this._RoomRepository.getRoomById(roomId);
    if (room) {
      return room.inGame;
    }
    return null;
  }

  isRoomActive(roomId) {
    const room = this._RoomRepository.getRoomById(roomId);
    if (room) {
      return room.isActive;
    }
    return null;
  }

  removeUserFromRoomById(userId) {
    const room = this.getRoomByUserId(userId);
    this._RoomRepository.update(room.id, {
      ...room,
      users: room.users.filter((user) => user !== userId),
    });
  }

  addUserToRoomById(userId, roomId) {
    const room = this._RoomRepository.getRoomById(roomId);
    const active =
      room.users.length + 1 === config.MAXIMUM_USERS_FOR_ONE_ROOM
        ? false
        : true;
    this._RoomRepository.update(roomId, {
      ...room,
      users: [...room.users, userId],
    });
  }

  addWinnersToRoom(userId, roomId) {
    if (this.winners.get(roomId)) {
      const array = this.winners.get(roomId);
      array.push(userId);
      this.winners.set(roomId, array);
    } else {
      this.winners.set(roomId, [userId]);
    }
  }

  deleteWinnersToRoom(userId, roomId) {
    this.winners[roomId] = this.winners[roomId].filter((el) => el !== userId);
  }

  clearWinners(roomId) {
    this.winners.set(roomId, []);
  }

  getWinners(roomId) {
    return this.winners.get(roomId);
  }

  hasWinner(roomId, userId) {
    if (this.winners.has(roomId)) {
      const winnerArray = this.winners.get(roomId);
      return winnerArray.some((user) => user === userId);
    } else {
      return false;
    }
  }

  setBot(roomId, botComentator) {
    RoomRepository.getRoomById(roomId).bot = botComentator;
  }

  getBot(roomId) {
    const room = RoomRepository.getRoomById(roomId);
    if (room) {
      return room.bot;
    }
    return null;
  }
}

export default new RoomService(RoomRepository);
