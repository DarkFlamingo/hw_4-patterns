import * as config from '../config';
import { texts } from '../../data';
import { botComentator } from '../bot';
import UserService from '../services/user.service';
import RoomService from '../services/room.service';
import ResultService from '../services/result.services';
import Timer from '../services/timer.service';

const isExistingRoom = (id) => {
  const room = RoomService.getRoomById(id);
  if (room) {
    return true;
  } else {
    return false;
  }
};

const getRandomTextId = () => {
  if (texts.length) {
    const min = Math.ceil(0);
    const max = Math.floor(texts.length);
    return Math.floor(Math.random() * (max - min)) + min;
  } else {
    return null;
  }
};

const getCurrentRoomId = (socket) => {
  return Object.keys(socket.rooms).find((room) => isExistingRoom(room));
};

export const roomEvents = (io, socket) => {
  const username = socket.handshake.query.username;

  if (!UserService.isUserExist(username)) {
    UserService.addUser(username);
  } else {
    socket.emit('USER_EXIST');
  }
  socket.emit('UPDATE_ROOMS', RoomService.getAllActiveRooms());

  return {
    disconnect: () => {
      const user = UserService.getUserById(username);
      if (user) {
        const room = RoomService.getRoomByUserId(username);
        let active = true;
        if (room) {
          active = room.isActive;
        }
        UserService.deleteUser(username);
        if (room) {
          RoomService.removeUserFromRoomById(username);
          const updatedRoom = RoomService.getRoomById(room.id);
          io.to(updatedRoom.id).emit('JOIN_ROOM_DONE', {
            room: updatedRoom,
            users: RoomService.getAllUsersInRoom(updatedRoom.id),
          });
          if (RoomService.isEveryUserReady(updatedRoom.id) && active === true) {
            RoomService.toggleRoomActive(updatedRoom.id, false);
            io.to(updatedRoom.id).emit('EVERY_USER_READY', getRandomTextId());
          }
          if (RoomService.isEmptyRoom(updatedRoom.id)) {
            RoomService.deleteRoom(updatedRoom.id);
          }
        }
        io.emit('UPDATE_ROOMS', RoomService.getAllActiveRooms());
      }
    },

    createRoom: (roomId) => {
      if (RoomService.getRoomById(roomId)) {
        socket.emit('ROOM_EXIST');
      } else {
        const prevRoomId = getCurrentRoomId(socket);
        if (roomId === prevRoomId) {
          return;
        }
        if (prevRoomId) {
          socket.leave(prevRoomId);
          RoomService.removeUserFromRoomById(username, prevRoomId);
        }
        socket.join(roomId, () => {
          RoomService.addRoom(roomId);
          RoomService.setBot(roomId, botComentator(io, roomId));
          const roomBot = RoomService.getBot(roomId);
          if (roomBot) {
            roomBot.start();
          }
          RoomService.addUserToRoomById(username, roomId);
          io.emit('UPDATE_ROOMS', RoomService.getAllActiveRooms());
          io.to(roomId).emit('JOIN_ROOM_DONE', {
            room: RoomService.getRoomById(roomId),
            users: RoomService.getAllUsersInRoom(roomId),
          });
        });
      }
    },

    joinRoom: (roomId) => {
      const room = RoomService.getRoomById(roomId);
      if (room) {
        const prevRoomId = getCurrentRoomId(socket);
        if (roomId === prevRoomId) {
          return;
        }
        if (prevRoomId) {
          socket.leave(prevRoomId);
          RoomService.removeUserFromRoomById(username);
        }
        socket.join(roomId, () => {
          RoomService.addUserToRoomById(username, roomId);
          const roomBot = RoomService.getBot(roomId);
          if (roomBot) {
            roomBot.sendStartMessage();
          }
          io.emit('UPDATE_ROOMS', RoomService.getAllActiveRooms());
          io.to(roomId).emit('JOIN_ROOM_DONE', {
            room: RoomService.getRoomById(roomId),
            users: RoomService.getAllUsersInRoom(roomId),
          });
        });
      }
    },

    quit: (roomId) => {
      socket.leave(roomId);
      RoomService.removeUserFromRoomById(username, roomId);
      if (RoomService.isEmptyRoom(roomId)) {
        RoomService.deleteRoom(roomId);
      } else {
        io.to(roomId).emit('JOIN_ROOM_DONE', {
          room: RoomService.getRoomById(roomId),
          users: RoomService.getAllUsersInRoom(roomId),
        });
      }
      io.emit('UPDATE_ROOMS', RoomService.getAllActiveRooms());
      socket.emit('QUIT_DONE', null);
    },

    setUserReady: (roomId) => {
      UserService.toggleUserIsReady(username, true);
      io.to(roomId).emit('JOIN_ROOM_DONE', {
        room: RoomService.getRoomById(roomId),
        users: RoomService.getAllUsersInRoom(roomId),
      });
      if (RoomService.isEveryUserReady(roomId)) {
        RoomService.toggleRoomActive(roomId, false);
        io.to(roomId).emit('EVERY_USER_READY', getRandomTextId());
      }
      io.emit('UPDATE_ROOMS', RoomService.getAllActiveRooms());
    },

    setUserNotReady: (roomId) => {
      UserService.toggleUserIsReady(username, false);
      io.to(roomId).emit('JOIN_ROOM_DONE', {
        room: RoomService.getRoomById(roomId),
        users: RoomService.getAllUsersInRoom(roomId),
      });
    },

    startTimer: (roomId) => {
      RoomService.toggleRoomInGame(roomId, true);
      const roomBot = RoomService.getBot(roomId);
      if (roomBot) {
        roomBot.startRace(RoomService.getAllUsersInRoom(roomId));
      }
      Timer.stopTimer();
      Timer.startTimer(
        config.SECONDS_TIMER_BEFORE_START_GAME,
        (seconds) => {
          io.to(roomId).emit('TIMER_BEFORE', seconds);
        },
        () => {
          io.to(roomId).emit('START_GAME');
        }
      );
    },

    startGameTimer: (roomId) => {
      const room = RoomService.getRoomById(roomId);
      room.users.forEach((user) =>
        ResultService.addResult({
          id: user,
          keyCount: 0,
          keyAllCount: 1000,
          time: null,
        })
      );
      const roomBot = RoomService.getBot(roomId);
      if (roomBot) {
        roomBot.notificationOfProgressOn();
      }
      Timer.stopTimer();
      Timer.startTimer(
        config.SECONDS_FOR_GAME,
        (seconds) => {
          io.to(roomId).emit('GAME_TIMER', seconds);
        },
        () => {
          const roomBot = RoomService.getBot(roomId);
          if (roomBot) {
            roomBot.notificationOfProgressOff();
          }
          RoomService.toggleRoomInGame(roomId, false);
          io.to(roomId).emit('FINISH_GAME', RoomService.getWinners(roomId));
        }
      );
      Timer.setStart();
    },

    changeProgressBar: ({ room, username, keyCount, keyAllCount }) => {
      const percent = (keyCount / keyAllCount) * 100;
      const result = ResultService.getResultById(username);
      if (result) {
        ResultService.updateResult(username, {
          id: username,
          keyCount,
          keyAllCount,
          time: null,
        });
      } else {
        ResultService.addResult({
          id: username,
          keyCount,
          keyAllCount,
          time: null,
        });
      }
      const roomBot = RoomService.getBot(room.id);
      if (
        roomBot &&
        keyAllCount - keyCount < config.NUMBER_OF_LETTERS_TO_THE_END
      ) {
        roomBot.notifyAboutApproachingFinishLine(username);
      }
      io.to(room.id).emit('UPDATE_PROGRESS_BAR_FRONT', { username, percent });
      if (percent === 100 && !RoomService.hasWinner(room.id, username)) {
        if (roomBot) {
          roomBot.notifyFinish(username);
        }
        const updatedResult = ResultService.getResultById(username);
        ResultService.updateResult(username, {
          ...updatedResult,
          time: Timer.getTime(),
        });
        RoomService.addWinnersToRoom(username, room.id);
      }
    },

    everyoneDone: (roomId) => {
      RoomService.toggleRoomInGame(roomId, false);
      const roomBot = RoomService.getBot(roomId);
      if (roomBot) {
        roomBot.notificationOfProgressOff();
        roomBot.notifyFinishGame();
      }
      io.to(roomId).emit('FINISH_GAME', RoomService.getWinners(roomId));
    },

    resetGame: (roomId) => {
      RoomService.resetIsReady(roomId);
      if (
        RoomService.getRoomById(roomId).users.length <
        config.MAXIMUM_USERS_FOR_ONE_ROOM
      ) {
        RoomService.toggleRoomActive(roomId, true);
      }
      RoomService.clearWinners(roomId);
      Timer.stopTimer();
      io.emit('UPDATE_ROOMS', RoomService.getAllActiveRooms());
      io.to(roomId).emit('JOIN_ROOM_DONE', {
        room: RoomService.getRoomById(roomId),
        users: RoomService.getAllUsersInRoom(roomId),
      });
    },

    stopTimer: () => {
      Timer.stopTimer();
    },
  };
};
