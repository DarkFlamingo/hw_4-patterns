import { createElement } from './dom.helper.mjs';

export const createWinnerDiv = (winner, position) => {
  const element = createElement('div', `place-${position}`);
  element.innerText = `${position}.` + winner;
  return element;
};

export const createLetterSpan = (letter) => {
  const element = createElement('span', '');
  element.innerText = letter;
  return element;
};

export const createUserDiv = (user, username) => {
  const container = createElement('div', 'row progress-row', {
    id: `users-div-${user.id}`,
  });

  const infoWrapper = createElement('div', 'info-wrapper');
  const statusColor =
    user.isReady === true ? 'ready-status-green' : 'ready-status-red';
  const statusDiv = createElement('div', statusColor);
  const userName = createElement('strong', 'username');
  username === user.id
    ? (userName.innerText = user.id + ' (you)')
    : (userName.innerText = user.id);

  const progressDiv = createElement('div', 'progress');
  const progressBarDiv = createElement('div', `user-progress ${user.id}`, {
    style: 'width:0%;',
  });

  infoWrapper.appendChild(statusDiv);
  infoWrapper.appendChild(userName);
  progressDiv.appendChild(progressBarDiv);
  container.appendChild(infoWrapper);
  container.appendChild(progressDiv);

  return container;
};

export const createRoomButton = (roomId, cb) => {
  const roomButton = createElement('button', 'btn btn-primary', {
    type: 'button',
    id: roomId,
  });

  const onJoinRoom = cb;

  roomButton.addEventListener('click', onJoinRoom);

  roomButton.innerText = 'Join';

  return roomButton;
};

export const createQuitButton = (roomId, cb) => {
  const quitButton = createElement('button', 'btn btn-primary', {
    type: 'button',
    id: 'quit-room-btn',
  });

  const onQuit = cb;

  quitButton.addEventListener('click', onQuit);

  quitButton.innerText = 'Quit';

  return quitButton;
};

export const createRoomItem = (name, quantityOfUsers, cb) => {
  const roomDiv = createElement('div', 'room');

  const quanUsers = createElement('h6');
  let text;
  quantityOfUsers === 1
    ? (text = 'user connected')
    : (text = 'users connected');
  quanUsers.textContent = quantityOfUsers + ' ' + text;

  const roomName = createElement('h5');
  roomName.textContent = name;

  const btn = createRoomButton(name, cb);

  roomDiv.appendChild(quanUsers);
  roomDiv.appendChild(roomName);
  roomDiv.appendChild(btn);

  return roomDiv;
};
