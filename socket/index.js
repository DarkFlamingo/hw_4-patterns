import { roomEvents } from './events/room.events';

export default (io) => {
  io.on('connection', (socket) => {
    const event = roomEvents(io, socket);

    socket.on('disconnect', () => event.disconnect());
    socket.on('CREATE_ROOM', (roomId) => event.createRoom(roomId));
    socket.on('JOIN_ROOM', (roomId) => event.joinRoom(roomId));
    socket.on('QUIT', (roomId) => event.quit(roomId));
    socket.on('USER_READY', (roomId) => event.setUserReady(roomId));
    socket.on('USER_NOT_READY', (roomId) => event.setUserNotReady(roomId));
    socket.on('START_TIMER', (roomId) => event.startTimer(roomId));
    socket.on('START_GAME_TIMER', (roomId) => event.startGameTimer(roomId));
    socket.on('PROGRESS_BAR_CHANGED', (data) => event.changeProgressBar(data));
    socket.on('EVERYONE_DONE', (roomId) => event.everyoneDone(roomId));
    socket.on('STOP_TIMER', () => event.stopTimer());
    socket.on('RESET_GAME', (roomId) => event.resetGame(roomId));
  });
};
